# lab2_ssoo
*youtube-dl, descargar audio*

***Pre-requisitos***

- C++
- Make
- youtube-dl
-ffmpeg

***Instalación***

- _Instalar Make._

    abrir terminal y escriba el siguiente codigo:
    $sudo apt install make

- _Instalar youtube-dl._
    para instalar youtube-dl debe seguir las instrucciones de la siguiente pagina web: https://ytdl-org.github.io/youtube-dl/        download.html

- _Instalar ffmpeg._
    en la terminal escribir el siguiente codigo $sudo apt install ffmpeg

***Problematica***
- Se debe crear un programa que dada un URL de youtube, pueda descargary extraer el audio de este video a traves de la aplicacion yutube-dl y posteriormente se reproduzca este audio a traves de la terminal


***Ejecutando***

- Para ejecutar el programa debe tener todos los archivos de este repositorio en una misma carpeta, depues en la terminal usar el comando: **$_make_**, esperar que compile y ejecutar el programa con el siguiente comando: **_$./programa_**, ademas separado por un espacio debe entregar el link del video de youtube al que se desea extraer el audio

como por ejemplo: _$./programa https://www.youtube.com/watch?v=Eum4MFbiYiE
(un video bastante gracioso para probar)



***Construido con C++***

***Librerias***

-unistd.h
-sys/wait.h
-iostream


***Version 0.1***

***Autor***

Luis Rebolledo





