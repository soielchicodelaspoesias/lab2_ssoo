#include <unistd.h>
#include <sys/wait.h>
#include <iostream>

class Fork {
  private:
    pid_t pid;
    int segundos;
    char* link_youtube;

  public:
    // Constructor
    Fork (int seg, char* link_yutube) {
      link_youtube = link_yutube;
      segundos = seg;
      crear();
      ejecutarCodigo();
    }

    // Métodos.
    void crear() {
      // crea el proceso hijo.
      pid = fork();
    }

    void ejecutarCodigo () {
      // valida la creación de proceso hijo.
      if (pid < 0) {
        std::cout << "No se pudo crear el proceso ...";
        std::cout << std::endl;

      } else if (pid == 0) {
        std::cout << "Descargando video y extrayendo audio..." << std::endl;
        // Código del proceso hijo.
        std::cout << "ID Proceso hijo: " << getpid();
        // se utiliza execlp para descargar el video
        // los parametros -x, --extract-audio, --audio-format y mp3 es para que una vez descargado el video se extrae el audio de este en formato mp3
        // despues viene la variable link_youtube es cual es un string del link entregrado
        // el -o y audio_video.mp3 es para guardar el audio con ese nombre
        execlp("youtube-dl","-x","--extract-audio","--audio-format","mp3", link_youtube, "-o", "audio_video.mp3", NULL);

        // Espera "segundos" para continuar (para pruebas).
        sleep(segundos);

      } else {
        // Código proceso padre.
        // Padre espera por el término del proceso hijo.
        wait (NULL);
        // Se termina el proceso hijo
        // se continua con el proceso Padre
        std::cout << "reproduciendo audio" << std::endl;
        // se utiliza execlp para reproducir el audio con ffplay
        execlp("ffplay", "nodisp", "audio_video.mp3", NULL);

      }
    }
};

int main (int argc, char *argv[]) {
  // se obtiene el link entregrado por terminal
  char* link_youtube = argv[1];
  // se instancia la clase fork
  Fork miFork(1, link_youtube);

  return 0;
}
